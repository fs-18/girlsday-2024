/*
 
  Folgender Code ist Teil des Projektes "Kein Blödsinn mit Lödzinn", welches im Rahmen des Girlsdays 2024 von Mitgliedern der FS 18 erstellt wurde.
  Bei der Hardware handelt es sich um ein Pendulum, welches von einem externen Motor angetrieben ist und über 17 LEDs verfügt, welche über einen Mikrocontroller "Arduino Nano Every" gesteuert werden.
  Folgende Funktionen haben die Messung verschiedener Eigenschaften wie z.B. die Umlaufdauer und darauf aufbauend die Ansteuerung der einzelnen LEDs zu bestimmten Zeit bzw Wegpunkten zur Aufgabe.
 
 */ 

// Zuerst erfolgt die Definition der globalen Variablen, welche in allen Funktionen genutzt werden können

  #define SEGMENTS          60        // Anzahl der Unterteilungen eines Umlaufs
  #define SENSOR_PIN        2         // Hall-Sensor ist mit Eingang D2 verbunden
  #define OFFSET            10        // Gleicht Verschiebungen aus, da LEDs und Hall-Sensor nicht direkt gegenüber sind
  
  
  volatile unsigned long last_time;   // speichert die Zeit ab, bei der der Hall-Sensor das letzte Mal ausgelöst wurde (Endzeit des Umlaufs)
  volatile unsigned long before_time; // speichert die Zeit,bei der der Hall-Sensor zuvor ausgelöst wurde (Anfangszeit des Umlaufs)
  volatile unsigned long period;      // Länge eines Umlaufs
  volatile bool rotation_start=0;     //wird zu true gesetzt, wenn der Hall-Sensor passiert wird und danach wieder auf false

  unsigned int segment = 0;           // Aktuell vorliegende Unterteilung; zählt durch
  unsigned int revolution = 0;        // Anzahl der ausgeführten Umläufe

  bool LED = 0;

  unsigned long led_test[SEGMENTS] = { // Beispiel Speicherinhalt, wenn sich die Platine nicht dreht
    0b00000000000000001,
    0b00000000000000010,
    0b00000000000000100,
    0b00000000000001000,
    0b00000000000010000,
    0b00000000000100000,
    0b00000000001000000,
    0b00000000010000000,
    0b00000000100000000,
    0b00000001000000000,
    0b00000010000000000,
    0b00000100000000000,
    0b00001000000000000,
    0b00010000000000000,
    0b00100000000000000,
    0b01000000000000000,
    0b10000000000000000,
    0b00100000000000000,
    0b00010000000000000,
    0b00001000000000000,
    0b00000100000000000,
    0b00000010000000000,
    0b00000001000000000,
    0b00000000100000000,
    0b00000000010000000,
    0b00000000001000000,
    0b00000000000100000,
    0b00000000000010000,
    0b00000000000001000,
    0b00000000000000100,
    0b00000000000000010,
    0b00000000000000001,
    0b00000000000000000,
    0b01010101010101010,
    0b10101010101010101,
    0b01010101010101010,
    0b10101010101010101,
    0b00000000000000000,
    0b11111111000000000,
    0b00000000111111111,
    0b11110000111100000,
    0b00001111000011111,
    0b11111111111111111,
    0b00000001100000000,
    0b00000010010000000,
    0b00000100001000000,
    0b00001000000100000,
    0b00010000000010000,
    0b00100000000001000,
    0b01000000000000100,
    0b10000000000000010,
    0b01000000000000100,
    0b00100000000001000,
    0b00010000000010000,
    0b00001000000100000,
    0b00000100001000000,
    0b00000010010000000,
    0b00000001100000000,
    0b11111111111111111,
    0b10000000000000000
 };

 unsigned long fs18_logo[SEGMENTS] = { // Beispiel Speicherinhalt, wenn sich die Platine dreht. Stellt das FS 18 Logo dar.
   
    0b0000000000000000
    0b0000000000000000
    0b0000000000000010
    0b0000000000000111
    0b0000000000000101
    0b0000000000000101
    0b0000000000000111
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000111111110010
    0b0000100010010010
    0b0001100010011010
    0b0001000010001010
    0b0001000010001010
    0b0001100010011010
    0b0000100010010010
    0b0000111111110010
    0b0000000000000010
    0b0000000000000010
    0b0001111111111010
    0b0001100000000010
    0b0000110000000010
    0b0000011000000010
    0b0000001100000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0001111111111010
    0b0000000010001010
    0b0000000010001010
    0b0000000010001010
    0b0000000010001010
    0b0000000000001010
    0b0000000000000010
    0b0000000000000010
    0b0000100111110010
    0b0000100010010010
    0b0001100010011010
    0b0001000010001010
    0b0001000010001010
    0b0001100010011010
    0b0000100010010010
    0b0000111110010010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000111
    0b0000000000000101
    0b0000000000000101
    0b0000000000000111
    0b0000000000000010
    0b0000000000000000
    0b0000000000000000

 };
    /*
    0b0000000000000000
    0b0000000000000000
    0b0000000000000010
    0b0000000000000111
    0b0000000000000101
    0b0000000000000101
    0b0000000000000111
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000111110010010
    0b0000100010010010
    0b0001100010011010
    0b0001000010001010
    0b0001000010001010
    0b0001100010011010
    0b0000100010010010
    0b0000100011110010
    0b0000000000000010
    0b0000000000000010
    0b0000000000001010
    0b0000000010001010
    0b0000000010001010
    0b0000000010001010
    0b0000000010001010
    0b0001111111111010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000011000010
    0b0000000001100010
    0b0000000000110010
    0b0000000000011010
    0b0001111111111010
    0b0000000000000010
    0b0000000000000010
    0b0000111111110010
    0b0000100010010010
    0b0001100010011010
    0b0001000010001010
    0b0001000010001010
    0b0001100010011010
    0b0000100010010010
    0b0000111111110010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000010
    0b0000000000000111
    0b0000000000000101
    0b0000000000000101
    0b0000000000000111
    0b0000000000000010
    0b0000000000000000
    0b0000000000000000
    */

// Die Setup-Funktion wird einmal ausgeführt, wenn die Reset Taste gedrückt wird oder die Energieversorgung eingeschaltet wird
void setup() {

// Folgede Zeilen definieren, ob die Anschlüsse des Arduino als Ausgnag oder Eingang genutzt werden
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(A5, OUTPUT);
  pinMode(SENSOR_PIN, INPUT);

  // Die Funktion "interrupt" wird immer bei einer fallenden Flanke des Ausgangs des Hall-Sensors ausgeführt
  attachInterrupt(digitalPinToInterrupt(2), interrupt, FALLING);

// Damit der Code im Loop-Teil erst bei Beginn eines neuen Umlaufs ausgeführt wird, stoppt folgende Schleife den Ablauf, bis der Hall-Sensor erneut auslöst
  while (! rotation_start) delay(1);

}



// Die Loop-Funktion wird immer wieder und wieder ausgeführt
void loop() {
  
  test_rotation();

//===========================================================
 
 Set_Led(led_test[segment]);         
 
 /* Der hier eingetragene Code beschreibt, wann welche LED leuchten soll. 
 Es kann die Funktion Set_Led genutzt werden oder eine andere Funktion von den unten aufgelisteten. 
 Es ist natürlich auch möglich noch weiter Funktionen selbst zu schreiben.
 */

//===========================================================
  
  wait_for_segment(segment+1);      // sorgt dafür, dass die Loop-Funktion erst für das nächste Segment wiederholt wird

}


//=============================== folgende Funktionen werden in der Setup- und Loop-Funktion aufgerufen ================


void interrupt(){                    
  /* setzt rotation_start auf true, speichert die Startzeit des vorangegangenen Umlaufs in before_time und 
  die Startzeit des aktuellen Umlaufs in last_time. Die Umlaufdauer "periode" wird durch die Differenz der beiden Zeiten
  */
    rotation_start = true;
    before_time = last_time;
    last_time = micros();    
    period = last_time-before_time;
    
}

void test_rotation(){                 
  /* Testet, ob man am Beginn des Umlaufs ist und falls dies der Fall ist wird rotation_start auf false gesetzt, 
  der Counter "segmente" auf den Anfangswert "Offset" gesetzt und der Counter "revolution" hochgesetzt
  */
  if (rotation_start) {
    rotation_start=false;
    segment = OFFSET;
    revolution++;
  }
}

void wait_for_segment(int next){      // Funktion zum timen der Ausführung der Loop-Funktion
    long wait_time = 0;

    segment = ++segment%SEGMENTS;
    wait_time = (micros() - last_time);
    wait_time = (period/SEGMENTS)  * (segment) - wait_time;

    if (wait_time < 0 || wait_time > period/SEGMENTS) {
      wait_time = period/SEGMENTS;
    }

    delay(wait_time/1000);
    delayMicroseconds(wait_time%1000); //maximal 16384us
}

//================folgende Funktionen sind Beispiele, was im Hauptteil der Loop-Funktion ausgeführt werden kann ============

void Set_Led(unsigned long led) {   

/* weist den Anschlüssen des Arduino die Werte in der Klammer "led" zu. 
Wird ein Speicher eingetragen, können verschiedene Werte an den verschiedenen Segmenten aufgerufen werden. 
Trägt man einen konstanten Wert wie z.B. Set_Led(0b0001001001), kann man Ringe3 erzeugen oder alle Lampen aufleuchten lassen.
Bei einer 0 ist die LED aus.
Bei einer 1 ist die LED an.
*/

  digitalWrite(A5,  0x1L&led>>0);    // Vergleicht, ob die das ganz rechte Bit der Binärzahl 1 oder 0 ist
  digitalWrite(A4,  0x1L&led>>1);    // Vergleicht, ob das 2. Bit von rechts eine 0 oder 1 ist
  digitalWrite(A3,  0x1L&led>>2);    // Vergleicht, ob das 3. Bit von rechts eine 0 oder 1 ist
  digitalWrite(A2,  0x1L&led>>3);    // Vergleicht, ob das 4. Bit von rechts eine 0 oder 1 ist
  digitalWrite(A1,  0x1L&led>>4);    // Vergleicht, ob das 5. Bit von rechts eine 0 oder 1 ist
  digitalWrite(A0,  0x1L&led>>5);    // Vergleicht, ob das 6. Bit von rechts eine 0 oder 1 ist
  digitalWrite(13,  0x1L&led>>6);    // Vergleicht, ob das 7. Bit von rechts eine 0 oder 1 ist
  digitalWrite(12,  0x1L&led>>7);    // Vergleicht, ob das 8. Bit von rechts eine 0 oder 1 ist 
  digitalWrite(11,  0x1L&led>>8);    // Vergleicht, ob das 9. Bit von rechts eine 0 oder 1 ist
  digitalWrite(10,  0x1L&led>>9);    // Vergleicht, ob das 10. Bit von rechts eine 0 oder 1 ist
  digitalWrite(9,  0x1L&led>>10);    // Vergleicht, ob das 11. Bit von rechts eine 0 oder 1 ist
  digitalWrite(8,  0x1L&led>>11);    // Vergleicht, ob das 12. Bit von rechts eine 0 oder 1 ist
  digitalWrite(7,  0x1L&led>>12);    // Vergleicht, ob das 13. Bit von rechts eine 0 oder 1 ist
  digitalWrite(6,  0x1L&led>>13);    // Vergleicht, ob das 14. Bit von rechts eine 0 oder 1 ist
  digitalWrite(5,  0x1L&led>>14);    // Vergleicht, ob das 15. Bit von rechts eine 0 oder 1 ist
  digitalWrite(4,  0x1L&led>>15);    // Vergleicht, ob das 16. Bit von rechts eine 0 oder 1 ist
  digitalWrite(3,  0x1L&led>>16);    // Vergleicht, ob das 17. Bit von rechts eine 0 oder 1 ist
}


void flash() {
  // schaltet alle LED bei jedem 10. Umlauf an.

  if(revolution%10==0) {
    Set_Led(0b11111111111111111);
  }
  else {
    Set_Led(0b00000000000000000);
  }

}

void pulse() {
  // durchläuft alle Ringe von außen nach innen

  if(revolution%17==0) {
    Set_Led(0b00000000000000001);
  }
  else if(revolution%17==1){
    Set_Led(0b00000000000000010);
  }
  else if(revolution%17==2){
    Set_Led(0b00000000000000100);
  }
   else if(revolution%17==3){
    Set_Led(0b00000000000001000);
  }
   else if(revolution%17==4){
    Set_Led(0b00000000000010000);
  }
   else if(revolution%17==5){
    Set_Led(0b00000000000100000);
  }
   else if(revolution%17==6){
    Set_Led(0b00000000001000000);
  }
   else if(revolution%17==7){
    Set_Led(0b00000000010000000);
  } 
  else if(revolution%17==8){
    Set_Led(0b00000000100000000);
  } 
  else if(revolution%17==9){
    Set_Led(0b00000001000000000);
  } 
  else if(revolution%17==10){
    Set_Led(0b00000010000000000);
  } 
  else if(revolution%17==11){
    Set_Led(0b00000100000000000);
  } 
  else if(revolution%17==12){
    Set_Led(0b00001000000000000);
  } 
  else if(revolution%17==13){
    Set_Led(0b00010000000000000);
  } 
  else if(revolution%17==14){
    Set_Led(0b00100000000000000);
  } 
  else if(revolution%17==15){
    Set_Led(0b01000000000000000);
  } 
  else if(revolution%17==16){
    Set_Led(0b10000000000000000);
  } 

}

void alternate() {
  
/* Alternativ können auch die Werte für die LED direkt festgelegt werden z.B. über die folgende Funktion, aber auch über den Einsatz von Set_Led(). 
Diese sind dann auch durchgängig gleich.
HIGH bedeutet, dass die LED an ist.
LOW bedeutet, dass die LED aus ist.
*/

  digitalWrite(A5, HIGH);
  digitalWrite(A4, LOW);
  digitalWrite(A3, HIGH);
  digitalWrite(A2, LOW);
  digitalWrite(A1, HIGH);
  digitalWrite(A0, LOW);
  digitalWrite(13, HIGH);
  digitalWrite(12, LOW);
  digitalWrite(11, HIGH);
  digitalWrite(10, LOW);
  digitalWrite(9, HIGH);
  digitalWrite(8, LOW);
  digitalWrite(7, HIGH);
  digitalWrite(6, LOW);
  digitalWrite(5, HIGH);
  digitalWrite(4, LOW);
  digitalWrite(3, HIGH);
}

